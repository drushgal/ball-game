using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinishScript : MonoBehaviour
{
    // Плохое место, потому что я так и не разобрался, нужен здесь [SerializeField] или нет и зачем
    [SerializeField] public GameObject finish_text; 
    void Start()
    {
        // Хорошее место, потому что дебаг логи это всегда хорошо
        // У меня какое-то время не работал этот скрипт, поэтому они помогли мне
        Debug.Log("Начало");
        if (finish_text == null) 
        {
            Debug.LogError("finish_text не назначен в инспекторе!");
        }
        finish_text.SetActive(false);
    }

    void OnCollisionEnter2D (Collision2D coll) {
        // Нормальное место, но наверное есть способы завершить игру получше чем остановить время
        Debug.Log("Коллизия обнаружена!"); 
        if (coll.gameObject.tag == "Ball") {
            Debug.Log("Коллизия с Ball обнаружена!"); 
            Debug.Log(finish_text.name);
            finish_text.SetActive(true);
            Time.timeScale = 0;
        }
    }
}